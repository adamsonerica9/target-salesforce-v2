"""SalesforceV2 target sink class, which handles writing streams."""
import re
import time
import requests
from typing import Any, Dict, List, Optional, Callable, cast

from target_salesforce_v2.rest import Rest
from singer_sdk.sinks import RecordSink
import abc
from singer_sdk.plugin_base import PluginBase

from target_salesforce_v2.auth import OAuth2Authenticator
from backports.cached_property import cached_property


class MissingRequiredFieldException(Exception):
    pass

class NoCreatableFieldsException(Exception):
    pass

class SalesforceV2Sink(RecordSink, Rest):
    """SalesforceV2 target sink class."""

    def __init__(
        self,
        target: PluginBase,
        stream_name: str,
        schema: Dict,
        key_properties: Optional[List[str]],
    ) -> None:
        """Initialize target sink."""
        self._target = target
        self.create_custom_fields = bool(target.config.get("create_custom_fields", True))
        super().__init__(target, stream_name, schema, key_properties)


    @property
    def max_size(self):
        return 1

    @property
    def authenticator(self) -> OAuth2Authenticator:
        """Return a new authenticator object."""
        if self.config.get("sandbox"):
            token_url = "https://test.salesforce.com/services/oauth2/token"
        else:
            token_url = "https://login.salesforce.com/services/oauth2/token"
        return OAuth2Authenticator(self._target, token_url)

    api_version = "v55.0"

    @property
    def permission_set_ids(self):
        params = {"q": "SELECT Id FROM PermissionSet"}
        response = self.request_api("GET", endpoint="query", params=params, headers={"Content-Type": "application/json"})
        return [r["Id"] for r in response.json()["records"]]

    @property
    def name(self):
        raise NotImplementedError

    @property
    def endpoint(self):
        raise NotImplementedError

    @property
    def unified_schema(self):
        raise NotImplementedError

    def url(self, endpoint=None):
        if not endpoint:
            endpoint = self.endpoint
        instance_url = self.config.get("instance_url")
        if not instance_url:
            self.authenticator
            instance_url = self.authenticator.instance_url
        return f"{instance_url}/services/data/{self.api_version}/{endpoint}"

    def validate_input(self, record: dict):
        return self.unified_schema(**record).dict()

    @cached_property
    def sf_fields(self):
        sobject = self.request_api("GET", f"{self.endpoint}/describe/")
        return [f for f in sobject.json()["fields"]]

    @cached_property
    def sf_fields_description(self):
        fld = self.sf_fields
        fields = {}
        fields["createable"] = [
            f["name"] for f in fld if f["createable"] and not f["custom"]
        ]
        fields["custom"] = [
            f["name"] for f in fld if f["custom"]
        ]
        fields["createable_not_default"] = [
            f["name"]
            for f in fld
            if f["createable"] and not f["defaultedOnCreate"] and not f["custom"]
        ]
        fields["required"] = [
            f["name"]
            for f in fld
            if not f["nillable"] and f["createable"] and not f["defaultedOnCreate"]
        ]
        fields["external_ids"] = [f["name"] for f in fld if f["externalId"]]
        fields["pickable"] = {}
        for field in fld:
            if field["picklistValues"]:
                fields["pickable"][field["name"]] = [
                    p["label"] for p in field["picklistValues"] if p["active"]
                ]
        return fields

    def get_pickable(self, record_field, sf_field, default=None, select_first=False):
        pickable_fields = self.sf_fields_description["pickable"]
        if sf_field not in pickable_fields:
            return default
        valid_options = [re.sub(r'\W+', '', choice).lower() for choice in pickable_fields[sf_field]]
        nice_valid_options = [choice for choice in pickable_fields[sf_field]]

        if record_field not in valid_options:
            if select_first:
                self.logger.warning(
                    f"Using {nice_valid_options[0]} as {sf_field} {record_field} is not valid, valid values are {nice_valid_options}"
                )
                record_field = valid_options[0]
            else:
                record_field = default
        else:
            record_field = nice_valid_options[valid_options.index(record_field)]
        return record_field

    def sf_field_detais(self, field_name):
        fields = self.sf_fields
        return next((f for f in fields if f["name"] == field_name), None)

    def validate_output(self, mapping):
        mapping = self.clean_payload(mapping)
        payload = {}
        if not self.sf_fields_description["createable"]:
            raise NoCreatableFieldsException(f"No creatable fields for stream {self.name} stream, check your permissions")
        for k, v in mapping.items():
            if k.endswith("__c") or k in self.sf_fields_description["createable"] + ["Id"]:
                payload[k] = v

        # required = self.sf_fields_description["required"]
        # for req_field in required:
        #     if req_field not in payload:
        #         raise MissingRequiredFieldException(req_field)
        return payload

    def process_record(self, record: dict, context: dict) -> None:
        """Process the record."""

        # Getting custom fields from record 
        # self.process_custom_fields(record)

        fields = self.sf_fields_description

        for field in fields["external_ids"]:
            if record.get(field):
                try:
                    update_record = record.copy()
                    update_record.pop(field)
                    url = "/".join([self.endpoint, field, record[field]])
                    response = self.request_api(
                        "PATCH", endpoint=url, request_data=update_record
                    )
                    id = response.json().get("id")
                    self.logger.info(f"{self.name} updated with id: {id}")
                    return
                except:
                    self.logger.info(f"{field} with id {record[field]} does not exist.")

        if "Id" in record:
            if "ContactId" in record.keys():
                del record["ContactId"]
            id = record.pop("Id")
            url = "/".join([self.endpoint, id])
            response = self.request_api("PATCH", endpoint=url, request_data=record)
            response.raise_for_status()
            self.logger.info(f"{self.name} updated with id: {id}")
            return

        response = self.request_api("POST", request_data=record)
        try:
            id = response.json().get("id")
            self.logger.info(f"{self.name} created with id: {id}")
        except:
            pass

    def query_sobject(self, query, fields):
        params = {"q": query}
        response = self.request_api("GET", endpoint="query", params=params)
        response = response.json()["records"]
        return [{k: v for k, v in r.items() if k in fields} for r in response]

    def process_custom_fields(self, record) -> None:
        """ 
            Process the custom fields for Salesforce, 
            creating unexsisting custom fields based on the present custom fields available in the record.

            Inputs: 
            - record
        """

        # If the config.json does not specify to create the custom fields 
        # automatically, then just don't execute this function
        if not self.create_custom_fields:
            return None

        # Checking if the custom fields already exist in 
        salesforce_custom_fields = self.sf_fields_description['custom']

        for cf in record:
            cf_name = cf['name']
            if not cf_name.endswith('__c'):
                cf_name+='__c'
            if cf_name not in salesforce_custom_fields:
                # If there's a custom field in the record that is not in Salesforce
                # create it 
                self.add_custom_field(cf['name'],label = cf.get('label'))

        return None

    def add_custom_field(self,cf,label=None):
        if not label:
            label = cf

        if not cf.endswith('__c'):
            cf += '__c'
        # Getting token and building the payload
        access_token = self.http_headers['Authorization'].replace('Bearer ','')
        sobject = self.endpoint.replace('sobjects/','')

        if sobject == 'Task':
            # If it's a task's custom field we need to create it under 
            # `Activity` sObject, so we change `Task` -> `Activity`
            sobject = 'Activity'

        url = self.url("services/Soap/m/55.0").replace('services/data/v55.0/','')

        # If the new custom field is an external id it needs to contain 'externalid'
        external_id = 'true' if 'externalid' in cf.lower() else 'false'

        xml_payload = f"""<s:Envelope xmlns:s="http://schemas.xmlsoap.org/soap/envelope/">
                            <s:Header>
                                <h:SessionHeader xmlns:h="http://soap.sforce.com/2006/04/metadata" 
                                xmlns="http://soap.sforce.com/2006/04/metadata" 
                                xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" 
                                xmlns:xsd="http://www.w3.org/2001/XMLSchema">
                                <sessionId>{access_token}</sessionId>
                                </h:SessionHeader>
                            </s:Header>
                            <s:Body xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" 
                                xmlns:xsd="http://www.w3.org/2001/XMLSchema">
                                <createMetadata xmlns="http://soap.sforce.com/2006/04/metadata">
                                <metadata xsi:type="CustomField">
                                    <fullName>{sobject}.{cf}</fullName>
                                    <label>{label}</label>
                                    <externalId>{external_id}</externalId>
                                    <type>Text</type>
                                    <length>100</length>
                                </metadata>
                                </createMetadata>
                            </s:Body>
                        </s:Envelope>"""

        response = requests.request(
            method="POST",
            url=url,
            headers={'Content-Type':"text/xml","SOAPAction":'""'},
            data=xml_payload
        )
        self.validate_response(response)

        # update field permissions for custom field per profile
        if sobject == 'Activity':
            # But then, we need to add the permissions to the Task sObject
            # So we change it back again from `Activity` -> `Task`
            sobject = 'Task'
        for permission_set_id in self.permission_set_ids:
            self.update_field_permissions(permission_set_id, sobject_type=sobject, field_name=f"{sobject}.{cf}")

    def update_field_permissions(self,permission_set_id, sobject_type, field_name):

        payload = {
                        "allOrNone": True,
                        "compositeRequest": [
                            {
                                "referenceId": "NewFieldPermission",
                                "body": {
                                    "ParentId": permission_set_id,
                                    "SobjectType": sobject_type,
                                    "Field": field_name,
                                    "PermissionsEdit": "true",
                                    "PermissionsRead": "true"
                                },
                                "url": "/services/data/v55.0/sobjects/FieldPermissions/",
                                "method": "POST"
                            }
                        ]
                    }

        response = self.request_api("POST", endpoint="composite", request_data=payload, headers={"Content-Type": "application/json"})
