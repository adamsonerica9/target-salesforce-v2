"""SalesforceV2 target class."""

from singer_sdk import typing as th
from singer_sdk.target_base import Target

from singer_sdk.sinks import Sink
from typing import Type

from target_salesforce_v2.sinks import (
    CampaignSink,
    ContactsSink,
    DealsSink,
    CompanySink,
    RecurringDonationsSink,
    ActivitiesSink,
    CampaignMemberSink,
    FallbackSink
)

SINK_TYPES = [
    CampaignSink,
    ContactsSink,
    DealsSink,
    CompanySink,
    RecurringDonationsSink,
    ActivitiesSink,
    CampaignMemberSink,
]


class TargetSalesforceV2(Target):
    """Sample target for SalesforceV2."""

    MAX_PARALLELISM = 1

    def __init__(
        self,
        config=None,
        parse_env_config: bool = False,
        validate_config: bool = True,
    ) -> None:
        self.config_file = config[0]
        super().__init__(config, parse_env_config, validate_config)

    name = "target-salesforce-v2"
    config_jsonschema = th.PropertiesList(
        th.Property("client_id", th.StringType, required=True),
        th.Property("client_secret", th.StringType, required=True),
        th.Property("refresh_token", th.StringType, required=True),
        th.Property("sandbox", th.BooleanType, default=False),
    ).to_dict()

    def get_sink_class(self, stream_name: str) -> Type[Sink]:
        """Get sink for a stream."""
        for sink_class in SINK_TYPES:
            if sink_class.name.lower() == stream_name.lower():
                return sink_class
            
            # Search for streams with multiple names
            if stream_name.lower() in sink_class.available_names:
                return sink_class
        
        # Adds a fallback sink for streams that are not supported
        return FallbackSink
     

if __name__ == "__main__":
    TargetSalesforceV2.cli()
